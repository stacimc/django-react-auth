import React, { Component } from 'react';
import axios from 'axios';
import { Route, Switch } from 'react-router-dom';

import UsersList from './components/UsersList';
import NavBar from './components/NavBar';
import Form from './components/Form';
import Logout from './components/Logout';
import Message from './components/Message';

axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.withCredentials = true;


const applySetResult = (result) => (prevState) => ({
    users: result.results,
    pageCount: Math.ceil(result.count / 10),
});



class App extends Component {
  constructor() {
    super()
    this.state = {
      users: [],
      filterText: '',
      page: 1,
      pageCount: 0,

      title: 'Sample App',
      isAuthenticated: false,
      messageName: null,
      messageType: null
    }
  }

  componentDidMount() {
    this.getUsers();
  }

  onInitialSearch = (e) => {
    if (e) {
      e.preventDefault();
    }

    this.getUsers();
  }

  onFilteredSearch = (value) => {
    this.setState({filterText: value, page: 1}, this.getUsers)
  }

  handlePageClick = (data) => {
    // react-paginate gives 0-indexed pages, so increment by one
    let selected = data.selected + 1;
    this.setState({page:selected}, this.getUsers)
  }

  getUsers() {
    var url = `${process.env.REACT_APP_USERS_SERVICE_URL}/users?page=${this.state.page}`;
    if (this.state.filterText !== '') {
      url = url + `&username=${this.state.filterText}`;
    }

    axios.get(url)
    .then((res) =>  {
        this.setState(applySetResult(res.data))
    })
    .catch((err) => { console.log(err); })
  }

  logoutUser() {
    window.localStorage.clear();
    this.setState({ isAuthenticated: false });
  }

  loginUser(token) {
    window.localStorage.setItem('authToken', token);
    this.setState({ isAuthenticated: true });
    this.onInitialSearch();
    this.createMessage('Welcome!', 'success');
  }

  componentWillMount() {
    // clear page on reload
    this.setState({
      formData: {username: '', email: '', password: ''},
      username: '',
      email: ''
    });
    // set isAuth'd = true if there is a token in local storage
    if (window.localStorage.getItem('authToken')) {
      this.setState({ isAuthenticated: true });
    }
  }

  createMessage(name='Sanity Check', type='success') {
    this.setState({
      messageName: name,
      messageType: type
    })
    setTimeout(() => {
      this.removeMessage()
    }, 3000);
  }

  removeMessage() {
    this.setState({
      messageName: null,
      messageType: null
    })
  }

  render() {
    return (
      <div className="height-100">
        <NavBar 
          title={this.state.title}
          isAuthenticated={this.state.isAuthenticated}
        />
        <div className="container">
          {this.state.messageName && this.state.messageType &&
            <Message
              messageName={this.state.messageName}
              messageType={this.state.messageType}
              removeMessage={this.removeMessage.bind(this)}
            />
          }
        </div>
        <div className="container height-100">
          <div className="rowb height-100">
            <div className="col-md-6 height-100">
              <br/>
              <Switch>
                <Route exact path='/' render={() => (
                    <UsersList users={this.state.users}
                                onFilteredSearch={this.onFilteredSearch}
                                handlePageClick={this.handlePageClick}
                                pageCount={this.state.pageCount}
                                page={this.state.page}/>
                )} />
                <Route exact path='/register' render={() => (
                  <Form
                    formType={'register'}
                    isAuthenticated={this.state.isAuthenticated}
                    loginUser={this.loginUser.bind(this)}
                    createMessage={this.createMessage.bind(this)}
                  />
                  )} />
                <Route exact path='/login' render={() => (
                  <Form
                    formType={'login'}
                    isAuthenticated={this.state.isAuthenticated}
                    loginUser={this.loginUser.bind(this)}
                    createMessage={this.createMessage.bind(this)}
                  />
                  )} />
                <Route exact path='/logout' render={() => (
                  <Logout
                    logoutUser={this.logoutUser.bind(this)}
                    isAuthenticated={this.state.isAuthenticated}
                  />
                  )} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App;
