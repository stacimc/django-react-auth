import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import 'bootstrap3/dist/css/bootstrap.css'
//import '../../node_modules/react-star-rating/dist/css/react-star-rating.min.css'
import './App.css'

import App from './App.js';


ReactDOM.render((
  <Router>
    <App />
  </Router>
  ), document.getElementById('root'))
