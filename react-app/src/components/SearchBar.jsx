import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import FormErrors from './FormErrors';


class SearchBar extends Component {

  constructor (props) {
		super(props)

		this.handleChange = this.handleChange.bind(this);
	}

  handleChange() {
    this.props.onFilteredSearch(this.refs.filterTextInput.value);
  }

  render() {
    return (
      <form>
          <input
            type="text"
            placeholder="Search..."
            value={this.props.filterText}
            ref="filterTextInput"
            onChange={this.handleChange}
          />
      </form>
    )
  }

}

export default SearchBar;