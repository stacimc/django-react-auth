import React from 'react';
import SearchBar from './SearchBar'
import UserDetail from './UserDetail'
import { Grid } from 'react-bootstrap';

import ReactPaginate from 'react-paginate';


const UsersList = (props) => {
	return (
    <div className="height-100">
      <div>
        <h1>Users</h1>
        <div id="pagination">
          <ReactPaginate previousLabel={"previous"}
                         nextLabel={"next"}
                         breakLabel={<a href="">...</a>}
                         breakClassName={"break-me"}
                         pageCount={props.pageCount}
                         marginPagesDisplayed={2}
                         pageRangeDisplayed={5}
                         onPageChange={props.handlePageClick}
                         containerClassName={"pagination"}
                         subContainerClassName={"pages pagination"}
                         activeClassName={"active"}
                         forcePage={props.page - 1}/>
        </div>
      </div>
      <div id="usersListColumns" className="height-100">
        <div id="searchBar">
          <h3>Filters</h3>
          <SearchBar onFilteredSearch={props.onFilteredSearch}
                     filterText={props.filterText}/>
        </div>
        <div id="usersList" className="height-100">
            {
              props.users.map((user) => {
                return (
                  <UserDetail key={user.id} user={user} />
                )
              })
            }
            <br className="clear" />
        </div>
      </div>
    </div>
  )
}



export default UsersList;