import React from 'react';
import { Row, Col } from 'react-bootstrap';


const UserDetail = (props) => {
	return (

      <Row className="sitter-detail">
      {/*
       <Col xs={4} className="user-photo pull-left">
          <img src={props.user.image_url} alt="{props.user.username}" className="user-image"/>
       </Col>
       */}

        <Col xs={8} className="user-detail-body">
          <h1 className="user-title">
            {props.user.username}
          </h1>
          <br/><br/>
          <ul className="user-contact-info">
            <li>
              <label>Email: </label>
              <span>{props.user.email}</span>
            </li>
          </ul>
        </Col>
        <br className="clear" />

      </Row>
  )
}


export default UserDetail;