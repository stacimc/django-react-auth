import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import FormErrors from './FormErrors';

class Form extends Component {
	constructor (props) {
		super(props)
		this.state = {
			formData: {
				username: '',
				email: '',
				password: '',
				password2: '',
			},
			formRules: [
			    {
  			  	id: 1,
  			  	field: 'username',
  			  	name: 'Username must be at least 3 characters.',
  			  	valid: false
  			  },
  			  {
  			  	id: 2,
  			  	field: 'email',
  			  	name: 'Email must be greater than 5 characters.',
  			  	valid: false
  			  },
  			  {
  			  	id: 3,
  			  	field: 'email',
  			  	name: 'Email must be a valid email address.',
  			  	valid: false
  			  },
  			  {
  			  	id: 4,
  			  	field: 'password',
  			  	name: 'Password must be greater than 8 characters.',
  			  	valid: false
  			  },
  			  {
  			  	id: 5,
  			  	field: 'password',
  			  	name: 'Passwords must match.',
  			  	valid: false
  			  }
			],
			valid: false
		}
		this.handleUserFormSubmit = this.handleUserFormSubmit.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.initRules = this.initRules.bind(this);
	}

	componentDidMount() {
		this.clearForm();
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.formType !== nextProps.formType) {
			this.clearForm();
			this.initRules();
		}
	}

	clearForm() {
		this.setState({
			formData: {username: '', email: '', password: '', password2: ''}
		});
	}

	handleFormChange(event) {

		const obj = this.state.formData;
		obj[event.target.name] = event.target.value;
		this.setState(obj);
		this.validateForm();
	}

	handleUserFormSubmit(event) {
		event.preventDefault();
		const formType = this.props.formType;
		let data;
		if (formType === 'login') {
			data = {
				username: this.state.formData.username,
				password: this.state.formData.password
			}
		}
		if (formType === 'register') {
			data = {
			  username: this.state.formData.username,
				email: this.state.formData.email,
				password1: this.state.formData.password,
				password2: this.state.formData.password2
			}
		}
		const url = `${process.env.REACT_APP_USERS_SERVICE_URL}/rest-auth/${formType}/`
		axios.post(url, data)
		.then((res) => {
			this.clearForm();
			this.props.loginUser(res.data.auth_token);
		})
		.catch((err) => {
		  console.log(err);
			if (formType === 'login') {
				this.props.createMessage('User does not exist.', 'danger')
			}
			if (formType === 'register') {
				this.props.createMessage('That user already exists.', 'danger')
			}
		})
	}

	validateForm() {
		const formType = this.props.formType;
		const rules = this.state.formRules;

		const formData = this.state.formData;
		this.setState({valid: false});
		for (const rule of rules) {
			rule.valid = false;
		}
		if (formData.username.length >= 3) {
		  rules[0].valid = true;
		}
		if (formType == 'register') {
      if (formData.email.length > 5) {
        rules[1].valid = true;
      }
      if (this.validateEmail(formData.email)) {
        rules[2].valid = true;
      }
		} else if (formType == 'login') {
		  rules[1].valid = true;
		  rules[2].valid = true;
		}
		if (formData.password.length > 8) {
			rules[3].valid = true;
		}
		if (formType == 'login' || formData.password == formData.password2 && formData.password.length > 0) {
		  rules[4].valid = true;
		}
		this.setState({formRules: rules})
		if (this.allTrue()) {
			this.setState({valid: true});
		}
	}

	validateEmail(email) {
  		// eslint-disable-next-line
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}


	allTrue() {
		for (const rule of this.state.formRules) {
			if (!rule.valid) {
				return false;
			}
		}
		return true;
	}

	initRules() {
		const rules = this.state.formRules;
		for (const rule of rules) {
			rule.valid = false;
		}
		this.setState({formRules: rules});
	}

  render() {
	  if (this.props.isAuthenticated) {
	  	return <Redirect to='/' />;
	  }
	  return (
	    <div>
	      <h1 style={{'textTransform':'capitalize'}}>{this.props.formType}</h1>
	      <hr/><br/>
	      <FormErrors
	        formType={this.props.formType}
	        formRules={this.state.formRules}
	      />
	      <form onSubmit={(event) => this.handleUserFormSubmit(event)}>
	        <div className="form-group">
	          <input
	            name="username"
	            onChange={this.handleFormChange}
	            className="form-control input-lg"
	            type="text"
	            placeholder="Enter your username"
	            required
	            value={this.state.formData.username}
	          />
	        </div>
          <div className="form-group">
            {this.props.formType === 'register' &&
              <input
                name="email"
                className="form-control input-lg"
                type="email"
                placeholder="Enter an email address"
                required
                value={this.state.formData.email}
                onChange={this.handleFormChange}
              />
            }
          </div>
          <div className="form-group">
            <input
              name="password"
              className="form-control input-lg"
              type="password"
              placeholder="Enter a password"
              required
              value={this.state.formData.password}
              onChange={this.handleFormChange}
            />
          </div>
          {this.props.formType === 'register' &&
	        <div className="form-group">
            <input
              name="password2"
              className="form-control input-lg"
              type="password"
              placeholder="Retype your password to confirm"
              required
              value={this.state.formData.password2}
              onChange={this.handleFormChange}
            />
          </div>
          }
          <input
            id="thesubmitbutton"
            type="submit"
            className="btn btn-primary btn-lg btn-block"
            value="Submit"
            disabled={!this.state.valid}
          />
        </form>
	    </div>
	  )
	}
}

export default Form;
