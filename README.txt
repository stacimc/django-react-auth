This is a shell application for a dockerized django-react app with authentication configured.


How to build the app locally:

  docker-machine create —driver virtualbox <your machine name>
  eval $(docker-machine env <your machine name>

  # Set environment variables for the react service
  export REACT_APP_USERS_SERVICE_URL=http://$(docker-machine ip <your machine name>)

  # Build the app
  docker-compose -f local.yml up -d —-build


Then get your ip with "docker-machine ip <your machine name" to view the app in the browser.


To import data from reviews.csv:

  # Make sure that the django migrations ran (known issue with django container attempting to run migrations before the database is ready)
  docker-compose -f local.yml exec django python manage.py migrate

  # Run script to import data
  docker-compose -f local.yml exec django python manage.py importData


Testing:

  # Run the tests with:
  docker-compose -f local.yml exec django python manage.py test to run the tests




How to debug the django service:
 1. replace the command in local.yml with 'tail -f /dev/null' to keep it waiting
 2. docker exec -i -t <CONTAINER_ID> /bin/bash
 3. ./start.sh and see what the error is



 How to build locally (without docker)

 1. Build the django app
  a. cd djangoApp
  b. python3 -m virtualenv env
  c. source env/bin/activate
  d. pip install -r requirements.txt

