"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views import generic
from rest_framework.schemas import get_schema_view


from rest_framework_jwt.views import obtain_jwt_token

from rest_framework import routers
from users import views


urlpatterns = [

    url(r'^users/$', views.UserList.as_view(), name="users-list"),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name="user-detail"),

    url(r'^api/$', get_schema_view()),

    # Needed for django-rest-auth
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/register/?', include('rest_auth.registration.urls')),

    # JWT configuration with django-rest-auth
    url(r'^api-token-auth', obtain_jwt_token),

    url(r'^admin/', include(admin.site.urls)),
]