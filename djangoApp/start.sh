#!/bin/sh
if [ ! -d static/rest_framework ]; then
  mkdir static/rest_framework
  cp -R /usr/local/lib/python3.5/site-packages/rest_framework/static/rest_framework/* static/rest_framework/
fi
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
