from .models import User
from rest_framework import serializers

from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email

from rest_auth.registration.serializers import RegisterSerializer


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'image_url', )


# Custom auth serializers

class RegisterSerializer(RegisterSerializer):
    image_url = serializers.URLField(required=False)

    def get_cleaned_data(self):
        return {
            'image_url': self.validated_data.get('image_url', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        print(user)
        self.cleaned_data = self.get_cleaned_data()

        # save custom fields
        user.image_url = self.cleaned_data.get('image_url')
        print('image_url ' + user.image_url)

        adapter.save_user(request, user, self)
        setup_user_email(request, user, [])

        user.save()
        print(user)
        return user

