import json
from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework.test import APIRequestFactory

from users.models import User
from users.views import UserList, UserDetail



# class UserListCreateAPIViewTestCase(TestCase):
#     def setUp(self):
#         # Every test needs access to the request factory.
#         self.url = reverse("users-list")
#         self.view = UserList.as_view()
#         self.factory = APIRequestFactory()
#
#     def test_list_users(self):
#         User.objects.create(username='jacob', email='jacob@jacob.com', password='top_secret')
#
#         request = self.factory.get(self.url, {})
#         response = self.view(request)
#
#         self.assertEquals(len(response.data['results']), User.objects.count())
#
#
# class UserDetailAPIViewTestCase(TestCase):
    # def setUp(self):
    #     # Every test needs access to the request factory.
    #     self.view = UserDetail.as_view()
    #     self.factory = APIRequestFactory()
    #     self.user = User.objects.create(
    #         username='jacob', email='jacob@jacob.com', password='top_secret')
    #
    #     self.url = reverse("user-detail", kwargs={'pk':self.user.pk})
    #
    #
    # def test_delete_fails_without_auth(self):
    #     request = self.factory.delete(self.url, {})
    #     response = self.view(request, pk=self.user.pk)
    #
    #     self.assertEquals(401, response.status_code)
    #
    #
    # def test_delete_user(self):
    #      request = self.factory.delete(self.url, {})
    #      request.user = self.user
    #      response = self.view(request, pk=self.user.pk)
    #
    #      self.assertEquals(204, response.status_code)
    #      self.assertEquals(User.objects.count(), 0)
    #
    # def test_update_user(self):
    #     request = self.factory.put(self.url, {"name":"Harry"})
    #     request.user = self.user
    #     response = self.view(request, pk=self.user.pk)
    #
    #     user = User.objects.get(pk=self.user.pk)
    #     self.assertEquals(user.name, "Harry")
