from rest_framework import generics, permissions

from .models import User
from .serializers import UserSerializer
from .permissions import UserIsOwnerOrReadOnly

class UserList(generics.ListAPIView):
    """
    list:
    Return a paginated list of all the existing users

    Should not be able to create users directly, must go through registration
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    permission_classes = [
        permissions.AllowAny
    ]

    def get_queryset(self):
        """
        Optionally restricts the returned users ot those whose usernames start with
        the provided filter string
        """
        queryset = User.objects.all()

        filterText = self.request.query_params.get('username', None)
        if filterText is not None:
            queryset = User.objects.filter(username__startswith = filterText)

        return queryset


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    retrieve:
    Returns a detail view of the given user

    update:
    Updates the given user if they are the logged in owner

    destroy:
    Deletes the given user if they are the logged in user
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    permission_classes = [
        UserIsOwnerOrReadOnly
    ]