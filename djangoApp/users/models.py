from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
class User(AbstractUser):

    image_url = models.URLField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'users'

    def __str__(self):
        return u"%s" % self.username